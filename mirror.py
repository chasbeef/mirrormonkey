import json
from shell import Command
from os import stat, path


class GitRepo(object):

    def __init__(self, **kwargs):
        self.kwargs = kwargs
        for k in kwargs.keys():
            self.__setattr__(k, kwargs[k])
        self.repo_working_directory = self.__generate_repo_dir_name()
        self.full_path = None
        self.mirror_status = self.mirror_repo()

    def __validate_input(self):
        required_args = ['working_directory', 'source_repo', 'destination_repo']
        if len(set(self.kwargs.keys()).intersection(required_args)) == len(required_args):
            return True
        else:
            return False

    def __use_ssh(self):
        required_args = ['ssh_public', 'ssh_private']
        if len(set(self.kwargs.keys()).intersection(required_args)) == len(required_args):
            return True
        else:
            return False

    def __generate_repo_dir_name(self):
        return self.source_repo.split('/')[-1]

    @staticmethod
    def __validate_directory_on_disk(test_path):
        try:
            stat(test_path)
        except FileNotFoundError:
            return False
        return True

    def __perform_initial_clone(self, auth):
        if auth == 'HTTPS':
            clone = Command('cd {} && git clone --mirror {} {}'.format(self.working_directory, self.source_repo,
                                                                       self.repo_working_directory))
        else:
            clone = Command()
        print(clone)
        return clone.was_successful()

    def __pull_changes(self, auth):
        if auth == 'HTTPS':
            pull = Command('cd {} && git remote update'.format(self.full_path))
        else:
            pull = Command()
        print(pull)
        return pull.was_successful()

    def __push_changes(self, auth):
        if auth == 'HTTPS':
            push = Command('cd {} && git push --mirror {}'.format(self.full_path, self.destination_repo))
        else:
            push = Command()
        print(push)
        return push.was_successful()

    def mirror_repo(self):
        print("Validating input.")
        if not self.__validate_input():
            return False

        print("Checking if SSH should be used.")
        if self.__use_ssh():
            print('SSH keys present. Will be used to authenticate.')
            auth = 'SSH'
        else:
            print('No SSH keys in configuration. Will assume username & password is present in URLs or keys '
                  'present on machine.')
            auth = 'HTTPS'

        self.full_path = path.join(self.working_directory, self.repo_working_directory)

        print('Checking to ensure working directory {} exists.'.format(self.working_directory))
        if not self.__validate_directory_on_disk(self.working_directory):
            return False

        print('Checking to see if this repo has already been cloned to Mirror Monkey.')
        if not self.__validate_directory_on_disk(self.full_path):
            print('{} does not exist. Will perform a full bare clone.'.format(self.full_path))
            if not self.__perform_initial_clone(auth):
                print('Clone failed.')
                return False
        else:
            print('{} exists. Pulling changes.'.format(self.full_path))
            if not self.__pull_changes(auth):
                print('Updating local copy of repo failed.')
                return False

        print('Pushing changes to {}'.format(self.destination_repo))
        if not self.__push_changes(auth):
            return False

        return True

    def was_successful(self):
        return self.mirror_status






'''
working_directory
source_repo
destination_repo
ssh_public
ssh_private
'''


repo = GitRepo(working_directory='/Users/cberndt/src/',
               source_repo='git@bitbucket.org:chasbeef/wireman.git',
               destination_repo='git@bitbucket.org:chasbeef/wireman.git')

